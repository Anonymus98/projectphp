<?php

namespace test\Controllers;

use test\Models\Car;
/**
 * @brief CarController
 */
class CarController extends AbstractController
{
    /**
     * @brief
     * $return void
     */

    public function view()
    {
        echo $this->view->render('home/view.html', ['title'=>"11111aaaa"]);


    }

    /**
     *
     */
    public function test()
    {
        $car    = new Car();

        $params = $this->request->getPost();


        $result = $car->getCarElement( $params['brand'], $params['euro'] );

        echo $this->view->render('home/view.html', ['cars' => $result]);
    }

}