<?php

namespace test\Controllers;

use test\Libs\Request;

/*
 * @brief a Class that handles a an abstract function that is used
 *        in all other controllers
 */
abstract class AbstractController
{
    /**
     * @var \test\Controllers\CarController $request
     */
    protected $request;

    /**
     * @var
     */
    protected $view;

    public function __construct( Request $request, $twig )
    {
        $this->request = $request;

        $this->view = $twig;
    }


    /**
     * @brief The function gets overridden in every class
     *        that the class gets extended to
     * @return void
     */
    abstract function view();

}