<?php

namespace test\Models;
/**
 * @brief Represents the model of a car that
 *        will get used in the CarController
 */
class Car
{
    /**
     * @var array $carArray
     */
    private $carArray   = array(
        array(
            'brand'=>'mercedes',
            'year'=>1999,
            'euro'=>9,
            'model'=>'w210',
        ),
        array(
            'brand'=>'bmw',
            'year'=>1997,
            'euro'=>11,
            'model'=>'316',
        ),
        array(
            'brand'=>'alpha romeo',
            'year'=>1999,
            'euro'=>9,
            'model'=>'w210',
        ),

    );

    /**
     * @brief The function creates an array with the special values
     * @return array
     */
    public function getCarElement( $brand, $euro )
    {
        /**
         * $brief an array that gets returned
         *        with the special value
         *
         * @var array $result
         */
        $result = array();

        /**
         * @brief Runs trough all the element
         *        keys in the array
         */
        foreach ( $this->carArray as $key )
        {
            /**
             * @brief checks the carArray
             *      for the income values
             *
             */
            if ( ($brand == $key['brand']) && ($euro <= $key['euro']) )
            {
                array_push($result, $key);
            }
        }
        return $result;
    }



}