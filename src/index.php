<?php

require_once 'vendor/autoload.php';

use test\Libs\Request;

use test\Libs\Router;

use test\Controllers\CarController;

/**
 * @var object $request
 */
$request    = new Request();

/**
 * @var
 */
$loader     = new Twig_Loader_Filesystem('view');
/**
 * @var
 */
$twig = new Twig_Environment($loader, [
    'cache' => false,
]);

/**
 * @var object $router
 */
$router     = new Router( $request );

/**
 * @param \test\Libs\Router $controller
 */
$controller = $router->getController();

/**
 * @param \test\Libs\Router $action
 */
$action     = $router->getAction();

/**
 * @brief checks the controller if it a certain parameter
 */
switch( strtolower($controller) )
{
    case 'car':
        $car = new CarController( $request, $twig ) ;
        $car->$action();
        break;

}