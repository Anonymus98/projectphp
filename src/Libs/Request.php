<?php

namespace test\Libs;

/**
 * Class Request
 * @package Libs
 * @brief   Specific function for $_SERVER.
 */
class Request
{

    /**
     * @brief   function getting uri.
     *
     * @return array
     */
    public function getUri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * @brief   function getting query
     * of values after ? of the url.
     *
     * @return array
     */
    public function getPost()
    {
        return $_POST;
    }

}