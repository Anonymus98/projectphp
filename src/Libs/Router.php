<?php

namespace test\Libs;

use test\Libs\Request;

/**
 * @brief Modifies the uri input params from the Autoloader
 *        and creates a controller and a action to  use in the index file.
 *
 */
class Router
{
    /**
     * $var \test\Libs\Request $request
     */
    private $request;

    /**
     * @var string $controller
     */
    protected $controller;

    /**
     * @var string $action
     */
    protected $action;

    /**
     * @param \test\Libs\Request $request
     */
    public function __construct( $request )
    {
        $this->request  = $request;

        $this->parse();
    }

    /**
     *@brief The function sets the private
     *       attributes to the values of the  exploded Uri
     *
     * @return void
     */
    public function parse()
    {
        $parsedUri  = parse_url($this->request->getUri());

        $exploded   = explode('/',trim($parsedUri['path'],'/'));

        $this->controller = $exploded[0];

        if ( isset($exploded[1]) )
        {
            $this->action   = $exploded[1];
        }
        else {
            $this->action   = 'view';
        }

    }

    /**
     * @brief takes the protected attribute action
     *
     * @return void
     */
    public function getAction()
    {
        return  $this->action;
    }

    /**
     * @brief takes the protected attribute action
     * @return void;
     */
    public function getController()
    {
        return  $this->controller;
    }
}